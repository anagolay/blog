![Anagolay Logo](https://ipfs.anagolay.network/ipfs/QmZ6NAvdCPQdj2Hh2gkw25yTq967aJfhFdC65bKBqVLeHJ)

[Introduction](README.md)

# Articles

- [Multi-token contributions]()
  - [Milestone 1 - Verification Pallet](./articles/multi-token-community-contributions-for-verified-creators/milestone-1.md)
- [Workflow Generation Retro](./articles/workflows-generation_retrospective_and_performance_analysis.md)
- [Project Idiyanale phase 1]()
  - [Milestone 1](./articles/project-idiyanale-phase-1/milestone-1.md)
  - [Milestone 2](./articles/project-idiyanale-phase-1/milestone-2.md)
  - [Phase 1 Completed](./articles/project-idiyanale-phase-1/web-3-foundation-announcement.md)
